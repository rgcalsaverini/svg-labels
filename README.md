# Labels

A teeny-tiny service to create small labels ![](./doc/example.png).

That's it... Nothing more :)

You can test it out by using it's API:
`http://chupsy.calsaverini.com:8001/label/<TEXT>/<COLOR>/<HEIGHT>`

For example: `http://chupsy.calsaverini.com:8001/label/Label%20all%20the%20things!!/a10800/30`

