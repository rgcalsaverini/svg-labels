from xml.sax.saxutils import escape
from tempfile import NamedTemporaryFile
import os
import subprocess

label_svg = """
<svg
   xmlns:svg="http://www.w3.org/2000/svg"
   xmlns="http://www.w3.org/2000/svg"
   height="{height}px"
   width="{width}px"
   viewBox="0 0 {vb_width} 8"
   version="1.1"
   >
<rect fill="{bg_color}" width="{vb_width}" height="8" x="0" y="0" ry="1.3" />
<text font-family="monospace" font-size="6px">
  <tspan x="{vb_center}" y="5.8" font-family="monospace" text-align="center" text-anchor="middle">{text}</tspan>
</text>
</svg>
"""


def _calc_vb_width(text):
    return len(text) * 4.241 + 5


def _get_color(color, default):
    if len(color) not in [7, 6]:
        return default
    col_part = color[1:] if len(color) == 7 else color
    if any(p not in '1234567890abcdefABCDEF' for p in col_part):
        return default
    return f'#{color}' if len(color) == 6 else color


def _get_float(value, default):
    try:
        return float(value)
    except ValueError:
        return default


def _generate_svg_label(text, color, height):
    color = _get_color(color, '#aaeeff')
    text = escape(text)[:50]
    vb_width = _calc_vb_width(text)
    aspect_ratio = vb_width / 8.0
    height = min(50, _get_float(height, 25))
    width = height * aspect_ratio

    svg = label_svg.format(
        width=width,
        height=height,
        vb_width=vb_width,
        bg_color=color,
        text=text,
        vb_center=vb_width / 2.0,
    )

    return svg, height


def _convert_svg_to_png(svg_data: str, height: float):
    svg_file = NamedTemporaryFile()
    png_file = NamedTemporaryFile()

    try:
        with open(svg_file.name, 'w') as fp:
            fp.write(svg_data)
        height_str = str(min(50, int(height)))
        cmd = ['inkscape', '-z', '-h', height_str, svg_file.name, '-e', png_file.name]
        res = subprocess.call(cmd, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        if res != 0:
            return None

        with open(png_file.name, 'rb') as fp:
            return fp.read()
    finally:
        _close_and_remove_temp_file(svg_file)
        _close_and_remove_temp_file(png_file)


def _close_and_remove_temp_file(temp_file: NamedTemporaryFile):
    if not temp_file:
        return
    file_name = getattr(temp_file, 'name')
    try:
        temp_file.close()
    except OSError:
        pass
    if file_name and os.path.isfile(file_name):
        os.remove(file_name)


def get_label(text, color, height):
    svg, height = _generate_svg_label(text, color, height)
    return _convert_svg_to_png(svg, height)
