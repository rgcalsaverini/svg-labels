from flask import Blueprint, Response, Flask
from labels.label import get_label
from flask_caching import Cache

_cache_time = 60 * 60 * 24


def create_app(bp_cls):
    app = Flask(__name__)
    config = {
        'CACHE_TYPE': 'filesystem',
        'CACHE_DEFAULT_TIMEOUT': _cache_time,
        'CACHE_DIR': '/tmp/cache',
    }
    app.config.from_mapping(config)
    cache = Cache(app)
    app.register_blueprint(bp_cls(cache))
    return app


def create_api(cache: Cache):
    blueprint = Blueprint('api', __name__)

    @blueprint.route('/label/<text>/<color>/<height>')
    @cache.memoize(timeout=_cache_time)
    def label(text, color, height):
        return Response(get_label(text, color, height), headers={'Content-Type': 'image/png'})

    return blueprint
