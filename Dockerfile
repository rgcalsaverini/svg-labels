FROM python:3.7.3-slim

RUN apt-get update && apt-get install -y inkscape

COPY requirements.txt /
RUN pip3 install -r /requirements.txt && rm /requirements.txt

RUN mkdir -p /app

COPY start.sh /
COPY app.py /app
COPY labels /app/labels

CMD ["/start.sh"]