#!/bin/sh

gunicorn --chdir app \
  --workers 2 \
  --threads 4 \
  --worker-connections 200 \
  --max-requests 500 \
  --max-requests-jitter 20 \
  --bind 0.0.0.0:80 \
  --access-logfile - \
  --limit-request-line 150 \
  --limit-request-field_size 200 \
  app