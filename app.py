from labels.api import create_api, create_app

application = create_app(create_api)

if __name__ == '__main__':
    application.run(debug=True)
